# mld

![](https://img.shields.io/badge/written%20in-C%2B%2B-blue)

A COFF linker targeting Win32 PE.

Builds under MSVC and GCC.

Tags: PL

## Changelog

2015-08-01: r157
- Feature: Add `-Wno-duplicate-symbol` option to suppress duplicate symbol warning
- Feature: Support embedded relative relocations
- Performance enhancements (addressing some profiler hotspots)
- Fix an issue with unaligned casts
- [⬇️ mld_r157.zip](dist-archive/mld_r157.zip) *(111.17 KiB)*
- [⬇️ mld_r157.src.zip](dist-archive/mld_r157.src.zip) *(1.91 MiB)*


2015-07-03: r146
- Feature: Add `-q` option to suppress warning/fatal linker output
- Feature: Add `--compress-rdata` option to pack `.rdata` into `.text` (marks constants as executable, but saves some space)
- Feature: Support recursive `.a` archives
- Work on standardising toward an embeddable library interface
- Reduce duplicate symbol definition from fatal to warning
- Fix an issue parsing metadata entries in some `.a` archives (fixes linking with mingw-w64 `msvcrt.a` and `kernel32.a`)
- Fix an issue unnecessarily tripping up on unknown symbols from segments that aren't being linked in
- [⬇️ mld_r146.zip](dist-archive/mld_r146.zip) *(110.56 KiB)*
- [⬇️ mld_r146.src.zip](dist-archive/mld_r146.src.zip) *(1.91 MiB)*


2015-06-24: r121
- Feature: Support `.a` archives
- Feature: Coalesce sections
- Feature: Support `-mwindows` subsystem
- Feature: Working IAT with support for cygwin/mingw import libraries
- Feature: Smaller binaries: strip unreferenced sections, elide zero-vss sections, elide .bss storage
- Remove PE/COFF viewer features to focus on linking
- Fix an issue applying invalid relocations if sections were removed
- [⬇️ mld_r121.zip](dist-archive/mld_r121.zip) *(80.56 KiB)*
- [⬇️ mld_r121.src.zip](dist-archive/mld_r121.src.zip) *(1.75 MiB)*


2014-07-03: r85
- Initial public release
- [⬇️ mld_r85.tar.gz](dist-archive/mld_r85.tar.gz) *(13.95 KiB)*
- [⬇️ mld_r85.exe.zip](dist-archive/mld_r85.exe.zip) *(72.15 KiB)*

